import React from 'react';
import Sidebar from '../elements/Sidebar/Sidebar';
import Header from '../elements/Header/Header';
import PropTypes from 'prop-types';

const Layout = ({ children }) => {
  return(
    <div className="flex h-screen overflow-hidden">
      {Sidebar()}
      <div className="flex flex-col w-full">
        {Header()}
        <div className="p-8 h-full drop-shadow-xl">
          {children}
        </div>
      </div>
    </div>
  );
};

export default Layout;

Layout.propTypes = {
  children: PropTypes.object.isRequired,
};
