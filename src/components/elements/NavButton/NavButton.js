import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function NavButton({ children, link, icon, style }) {

  return (
    <>
      <Link to={link}>
        <div className={style}>
          {<img className="mr-2" src={icon}/>}
          {children}
        </div>
      </Link>
    </>
  );
}

NavButton.propTypes = {
  children: PropTypes.node.isRequired,
  icon: PropTypes.object.isRequired,
  link: PropTypes.string.isRequired,
  style: PropTypes.string.isRequired,
};
