import React from 'react';
import PropTypes from 'prop-types';
import './TextArea.css';


export default function TextArea({ name, containerStyle, label, labelStyle, placeholder, TextAreaStyle, onChange, value }){


  return (
    <div className={containerStyle}>
      <label className={labelStyle} htmlFor={name}>{label}</label>
      <textarea className={TextAreaStyle} name={name} onChange={onChange} placeholder={placeholder} value={value}/>
    </div>
  );
}
TextArea.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  inputStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  TextAreaStyle: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,

};
