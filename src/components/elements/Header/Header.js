import React from 'react';
import searchIcon from '../../../assets/search.png';
import chatIcon from '../../../assets/chat.png';
import acneIcon from '../../../assets/acne.png';
import notifIcon from '../../../assets/notif.png';

export default function Header(){

  const items = [
    { icon: searchIcon },
    { icon: chatIcon },
    { icon: notifIcon },
  ];

  return (
    <header className="px-8 py-4 bg-white text-[#475569] text-right w-full flex items-center justify-end font-bold  sticky top-0">
      <ul>
        {
          items.map((item, idx)=>{
            return(
              <li className="inline" key={idx}>
                <div className="float-left mr-2 p-2 rounded-full bg-gray-100">
                  <img src={item.icon} />
                </div>
              </li>
            );
          })
        }
      </ul>
      <div className="flex float-left pr-2">
        <img src={acneIcon}/>
        <p className="px-4 mx-2 font-md">Acne</p>
      </div>
    </header>
  );
}
