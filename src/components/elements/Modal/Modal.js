import React from 'react';
import trash from '../../../assets/trash.png';
import PropTypes from 'prop-types';


export default function Modal({ name, onDelete }){

  const [show, setShow] = React.useState(false);



  return (
    <>
      <button
        className="py-2 px-2" onClick={()=>setShow(true)}
      ><img src={trash}/>
      </button>

      {show ? (
        <div className="justify-center items-center flex overflow-x-hiden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
          <div className="relative w-auto my-6 mx-auto max-w-3xl">
            {/* content */}
            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-gray-50 ">
              {/* body */}
              <div className="relative p-6 m-6 flex-auto">
                <p className="my-4 text-slate-800 text-lg font-semibold leading-relaxed">apakah yakin menghapus {name} ?</p>
              </div>
              {/* footer */}
              <div className="flex itemss-center justify-end p-4 border-t border-solid border-slate-300">
                <button
                  className="px-2 py-1 m-2 bg-emerald-500 hover:bg-emerald-800 text-white font-semibold rounded"
                  onClick={() => {
                    setShow(false);
                    onDelete;
                  }}

                >ya</button>
                <button
                  className="px-2 py-1 m-2 bg-red-500 hover:bg-red-800 text-white font-semibold rounded"
                  onClick={()=>setShow(false)}
                >tidak</button>
              </div>
            </div>
          </div>
        </div>
      ): null}
    </>
  );
}




Modal.propTypes = {
  name: PropTypes.array.isRequired,
  onDelete: PropTypes.func.isRequired,
};
