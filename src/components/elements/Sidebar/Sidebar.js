import React from 'react';
import NavButton from '../NavButton/NavButton';
import { Link, useLocation } from 'react-router-dom';
import productIcon from '../../../assets/product.png';
import userIcon from '../../../assets/user.png';
import Logo from '../../../assets/logo.png';

export default function Sidebar(){

  const location = useLocation();
  const items = [
    { name: 'Product', icon: productIcon, link: './products' },
    { name: 'User', icon: userIcon, link: './users' }
  ];

  return (
    <nav className="bg-[#1e293b] w-3/12 h-screen p-2 sticky top-0">
      <Link to="/">
        <img className="p-2" src={Logo}/>
      </Link>
      <h3 className="px-2 py-4 text-slate-600 font-medium">PAGES</h3>
      {items.map((menu, idx)=>{
        return(
          <NavButton
            icon={menu.icon}
            key={idx}
            link={menu.link}
            style={`flex p-3 w-full items-center mb-2 text-slate-100 font-semibold${location.pathname.includes(menu.link)? 'bg-[#0F172A]' :''}`}>
            {menu.name}
          </NavButton>
        );
      })}
    </nav>
  );
}
