import React from 'react';
import PropTypes from 'prop-types';
import './Select.css';


export default function TextArea({ name, containerStyle, label, labelStyle, selectStyle, onChange, value }){


  return (
    <div className={containerStyle}>
      <label className={labelStyle} htmlFor={name}>{label}</label>
      <select className={selectStyle} name={name} onChange={onChange} value={value} >
        <option value="">select</option>
        <option value="Ready">ready</option>
        <option value="Barang Bekas">barang bekas</option>
        <option value="Pre-Order">pre order</option>
      </select>
    </div>
  );
}
TextArea.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  inputStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  selectStyle: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
