import React from 'react';
import PropTypes from 'prop-types';
import './InputField.css';


export default function InputField({ name, containerStyle, inputStyle, label, labelStyle, placeholder, type, value, onChange, disabled }){


  return (
    <div className={containerStyle}>
      <label className={labelStyle} htmlFor={name}>{label}</label>
      <input
        className={inputStyle}
        disabled={disabled}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        type={type}
        value={value}/>
    </div>
  );
}
InputField.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  inputStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
