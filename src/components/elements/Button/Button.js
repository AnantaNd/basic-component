import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';


export default function Button({ children, style, onClick, disabled }){


  return (
    <button className={style} disabled={disabled} onClick={onClick}>
      {children}
    </button>

  );
}
Button.propTypes= {
  children: PropTypes.string.isRequired,
  disabled: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.string.isRequired,
};
