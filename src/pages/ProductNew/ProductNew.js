import React from 'react';
import './NewProduct.css';
import Button from '../../components/elements/Button';
import InputField from '../../components/elements/InputField';
import Select from '../../components/elements/Select';
import TextArea from '../../components/elements/TextArea';
import { Link , useNavigate } from 'react-router-dom';


export default function ProductNew() {

  const navigate = useNavigate();

  const [name, setName] = React.useState('');
  const [price, setPrice] = React.useState(0);
  const [description, setDescription] = React.useState('');
  const [hasExpiry, setHasExpiry] = React.useState(true);
  const [expiryDate, setExpiryDate] = React.useState('');
  const [image, setImage] = React.useState('');
  const [category, setCategory] = React.useState();

  const uploadImg = async (e) => {
    const file = e.target.files[0];
    const base64 = await base64Convert(file);
    setImage(base64);
    // console.log(base64);
  };
  const base64Convert = (file) => {
    return new Promise((resoleve)=>{
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resoleve(fileReader.result);
      };
    });
  };

  const onSubmit = () => {
    const newItem =
    {
      id : 'sku'+Math.floor(Math.random() * 1000),
      name,
      price,
      image,
      description,
      isDeleted : false,
      category,
      expiryDate,
    };
    navigate('/products', { state: newItem });
  };



  return (
    <main className="new-product">
      <p className="title">Product / Add New Product</p>
      <hr className="devider"/>
      <div className="input-container">
        <InputField
          containerStyle="input"
          inputStyle="input__text"
          label="Product Name"
          labelStyle="input__label"
          name="product-name"
          onChange={(e)=>setName(e.target.value)}
          placeholder="Enter Product Name"
          type="text"
          value={name}/>
        <InputField
          containerStyle="input"
          inputStyle="input__text"
          label="Product Price"
          labelStyle="input__label"
          name="product-price"
          onChange={(e)=>setPrice(e.target.value)}
          placeholder="Enter Price"
          type="text"
          value={price}/>
        <Select
          containerStyle="select"
          label="Select Item"
          labelStyle="select__label"
          name="select-item"
          onChange={(e)=>setCategory(e.target.value)}
          selectStyle="select__items"
          value={category}/>
      </div>
      <div className="split">
        <TextArea
          containerStyle="textarea"
          label="Description"
          labelStyle="textarea__label"
          name="description"
          onChange={(e)=>setDescription(e.target.value)}
          placeholder="Enter Product Description"
          TextAreaStyle="textarea__text"
          value={description}
        />
        <div className="input__fileUpload">
          <div className="image-preview">
            <img className="image" src={image}/>
          </div>
          <label className="input-tag"> Upload Image
            <input
              id="inputTag"
              onChange={(e)=>{uploadImg(e);}}
              type="file"
            />
          </label>
        </div>
      </div>
      <input
        checked={hasExpiry}
        className="checkbox"
        onChange={(e)=>setHasExpiry(e.target.checked)}
        type="checkbox"/>hide expired date
      <InputField
        containerStyle="input"
        disabled={hasExpiry ? false : true}
        inputStyle="input__date"
        label="Date Expiry"
        labelStyle="input__label"
        name="date-expiry"
        onChange={(e)=>setExpiryDate(e.target.value)}
        type="date"
        value={expiryDate}/>
      <div className="form__action">
        <Link to="/products/">
          <Button style="btn--secondary" >cancel</Button>
        </Link>
        <Button
          onClick={onSubmit}
          style="btn--primary" >save</Button>
      </div>
    </main>
  );
}
