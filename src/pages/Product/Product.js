/* eslint-disable no-console */
import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import defaultData from './product.json';
import Table from '../../components/elements/Table';
import Modal from '../../components/elements/Modal/Modal';

export default function Product(){

  const { state } = useLocation();
  const [data, setData] = useState(defaultData);
  const [category, setCategory] = useState('');
  const [isChecked, setIsChecked] = useState(false);


  useEffect(()=>{
    setData(state !== null ? [ state, ...data ] : [...data]);
  },[]);


  const onChangeSelect = (e) => {

    setCategory(e.target.value);
    if(e.target.value && !isChecked){
      console.log(isChecked);
      const filteredData = defaultData.filter((i) => (i.category === e.target.value));
      setData(filteredData);
    }else if(e.target.value && isChecked){
      console.log(isChecked);
      const filteredData = defaultData.filter((i) => (i.category === e.target.value && i.expiryDate > date));
      setData(filteredData);
    }else{
      setData(defaultData);
    }

  };

  const onChangeCheckBox = (e) => {
    setIsChecked(e.target.checked);
    if(e.target.checked && category === ''){
      console.log(e.target.checked);
      console.log(category);
      const filteredData = defaultData.filter((i) => (i.expiryDate > date));
      setData(filteredData);
    }else if(e.target.checked && category === 'Ready'){
      console.log(e.target.checked);
      console.log(category);
      const filteredData = defaultData.filter((i) => (i.expiryDate > date && i.category === 'Ready'));
      setData(filteredData);
    }else if(e.target.checked && category === 'Pre-Order'){
      console.log(e.target.checked);
      console.log(category);
      const filteredData = defaultData.filter((i) => (i.expiryDate > date && i.category === 'Pre-Order'));
      setData(filteredData);
    }else if(e.target.checked && category === 'Barang Bekas'){
      console.log(e.target.checked);
      console.log(category);
      const filteredData = defaultData.filter((i) => (i.expiryDate > date && i.category === 'Barang Bekas'));
      setData(filteredData);
    }else{
      setData(defaultData);
    }

  };

  const onDeleteHandler = (id) => {
    const filteredData = data.map((i) => {
      if(i.id === id){
        i.isDeleted = true;
        console.log(i);
      }
      return i;
    });
    setData(filteredData);

  };



  const columns = [
    'Product name',
    'Description',
    'Product price',
    'Category',
    'expiry date',
    'Action',
  ];

  const date = new Date().toISOString().slice(0,10);
  const price = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  const rowsData =(i, idx)=>{
    return(
      <tr className={i.expiryDate <= date ? 'p-5 text-slate-900 text-opacity-40':'p-5 text-slate-900 '} key={idx} >
        <td className="p-5 ">
          <Link to={`/products/${i.id}`}>
            <img className="rounded-full w-14 m-2 flex" src={i.image} />
            <p className="p-5 max-w-2xl">{i.name}</p>
          </Link>
        </td>
        <td className="p-5 max-w-2xl text-justify">{i.description}</td>
        <td className={i.expiryDate <= date ? 'p-5 text-green-500 text-opacity-40':'p-5 text-green-500 '}>Rp. {price(i.price)}</td>
        <td className="p-5 max-w-2xl">{i.category}</td>
        <td className="p-5 max-w-2xl">{i.expiryDate}</td>
        <td className="p-5" onClick={()=>onDeleteHandler(i.id)}><Modal name={i.name}/></td>
      </tr>
    );
  };


  return(
    <div className="p-8 bg-white h-full overflow-y-auto">
      <div className="float-right space-x-5" >
        <input
          checked={isChecked}
          className="p-2 m-2"
          onChange={onChangeCheckBox}
          type="checkbox"/>hide expired date
        <select className="p-2 h-10 rounded text-slate-800"
          onChange={onChangeSelect}
          onvalue={category}>
          <option value="">all category</option>
          <option value="Ready">Ready</option>
          <option value="Pre-Order">Pre order</option>
          <option value="Barang Bekas">Barang Bekas</option>
        </select>
        <Link to="/products/new">
          <button className="p-2 h-10 bg-blue-500 hover:bg-blue-700 text-white rounded ">
            Add Product
          </button>
        </Link>
      </div>
      <h4 className="mb-4 text-2xl font-bold text-slate-800">Product</h4>
      <hr className="mb-4"/>
      <Table columns={columns} items={data} rows={rowsData}/>
    </div>
  );
}
