import React from 'react';
import data from './user.json';
import Table from '../../components/elements/Table';
import Modal from '../../components/elements/Modal/Modal';

export default function User(){

  const columns=[
    'username',
    'email',
    'action'
  ];

  const rowsData = (i, idx) =>{
    return(
      <>
        <tr className="p-2 text-slate-800" key={idx}>
          <td className="p-2  flex just items-center"> <img className="rounded-full mr-3" src={i.profilePicture} width={70} />{i.username}</td>
          <td className="p-2">{i.email}</td>
          <td className="p-5"><Modal name={i.username}/></td>
        </tr>
      </>
    );
  };
  return(
    <div className="p-8 bg-white h-full overflow-y-auto">
      <h4 className="mb-4 text-2xl font-bold text-[#1E293B]">Users</h4>
      <hr className="mb-8"/>
      <Table columns={columns} items={data} rows={rowsData}/>
    </div>
  );
}
