import React from 'react';
import { useParams } from 'react-router-dom';
export default function ProductDetail(){

  const params = useParams( );
  // console.log( params );
  return(
    <main className="p-8 bg-white h-full">
      <h1 className="text-2xl text-black">{params.id}</h1>
    </main>

  );
}
