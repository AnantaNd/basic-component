import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader/root';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import ErrorBoundary from './components/elements/ErrorBoundary';
import { Main, Product, User, ProductDetail, ProductNew } from './pages';
import Layout from './components/Layout';





const App = ({ store }) => {
  return (
    <ErrorBoundary>
      <Provider store={store}>
        <BrowserRouter>
          {/* layout isi header dan navbar*/}
          <Layout >
            <Routes>
              <Route element={<Main />} exact path="/" />
              <Route element={<Product />} exact path="/products" />
              <Route element={<ProductDetail />} exact path="/products/:id" />
              <Route element={<User />} exact path="/users" />
              <Route element={<ProductNew />} exact path="/products/new" />
            </Routes>
          </Layout>
          {/* layout */}
        </BrowserRouter>
      </Provider>
    </ErrorBoundary>
  );
};

export default hot(App);

App.propTypes = {
  store: PropTypes.object.isRequired,
};
